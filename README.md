# **Používatelský manuál** #

Tento manuál slúži na správne používanie aplikácie.
## **Ako spustiť aplikáciu:** ##

* Pre vybuildovanie potrebujeme Qt Creator.

* Po otvorení projektu v Qt Creator prepneme build na Release:<br />
![](/images/qt_config.png)

* Pre správnu funkcionalitu spustíme skeletonTracker.exe:<br />
![](/images/skeleton.png)

* Taktiež je potrebné spustiť kobuki simulator:<br />
![](/images/kobuki.png)

* Následne klikneme na ikonu ![](/images/run.png) alebo použijeme klavesovú skratku **ctrl+r**

* Po úspešnom zbuildení a spustení by sme mali vidieť nasledovné okno:<br />
![](/images/okno.png)<br />

## **Ovládanie pohybu robota:** ##
### &nbsp;&nbsp;**Ovládanie pohybu pomocou klávesnice:** ###
- pohyb vpred je možný pomocou nasledovných klávesov: **W** a **8**
- pohyb vzad je možný pomocou nasledovných klávesov: **S** a **2**
- pohyb vpravo je možný pomocou nasledovných klávesov: **D** a **6**
- pohyb vlavo je možný pomocou nasledovných klávesov: **A** a **4**<br /><br />
![](/images/pohyb_klavesnica.png)

- pomocou medzerníka ovládame safety stop:<br />
![](/images/safety_stop.png)<br />Ak je aktivovaný safety stop nie je možné hýbať robotom. Na to aby sa obnovilo ovládanie je potrebné znova kliknúť medzerník alebo stlačiť tlačidlo Stop.

### &nbsp;&nbsp;**Ovládanie pohybu pomocou giest:** ###
* Ak nie je detegované žiadne gesto z tých ktoré sú zobrazené nižšie robot sa zastaví (za predpokladu že ho neovládame klávesnicou, vtedy je ovládanie klavesnicou nadriadené). Pohyb vľavo ovládame vystrčeným palcom pravej ruky, pre pohyb vpravo je to vice versa pohybu vľavo.
![](/images/gesta.png)

## **Vizualizácia varovaní:** ##

### **Zobrazenie vzdialeností robota od steny pomocou parkovacích senzorov:** ###
![](/images/senzory.png)<br />
Jednotlivé úrovne vzdialeností sú nasledovné:<br />

| Úroveň           	| 1  	| 2  	| 3  	| 4  	|
|:----------------	|:----:	|:----:	|:----:	|:----:	|
| Vzdialenosť [cm] 	| 60 	| 50 	| 40 	| 30 	|

### **Zobrazenie vzdialeností robota od steny pomocou fúzie dát z lidaru a dát z kamery:** ###
![](/images/fuzia.png)<br />
Jednotlivé úrovne vzdialeností sú nasledovné:<br />

| Obrázok       	| 1      	| 2            	| 3            	| 4            	| 5       	|
|:---------------	|:--------:	|:--------:	    |:--------:	    |:--------:	    |:--------:	|
| Interval [cm] 	| d >= 60 	| 60 < d <= 50 	| 50 < d <= 40 	| 40 < d <= 30 	|d < 30  	|

###  **Zobrazenie vzdialeností robota od steny pomocou dát z lidaru:** ###
![](/images/lidar.png)<br />
Úrovne vzdialeností sú zobrazené v tabulke vyššie:<br />

### **Zobrazenie varovaní o vzdialenostiach robota od steny pomocou dát z lidaru:** ###
![](/images/varovania.png)<br />
Jednotlivé úrovne vzdialeností sú nasledovné:<br />

| Obrázok       	| 1      	 | 2        |  
|:---------------	|:--------:	 |:--------:|
| Interval [cm] 	|28 < d <= 22,5| d < 22,5 	|

### **Zobrazenie informácií o smere pohybu:** ###
![](/images/pohyby.png)<br />