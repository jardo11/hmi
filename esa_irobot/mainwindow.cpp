#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QTimer"
#include "QPainter"
#include "math.h"
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/imgcodecs.hpp"
#include <QCoreApplication>
#include <QtConcurrent/QtConcurrent>
#include <QKeyEvent>

void MainWindow::initEncodersValues()
{
    enkoders.L_old = sens.EncoderLeft;
    enkoders.R_old = sens.EncoderRight;
    enkoders.Gyro_old = sens.GyroAngle + 17999;
}

void MainWindow::calculateActualAngle()
{
    signed short diff_angle = (enkoders.Gyro - enkoders.Gyro_old );
    (diff_angle > 7000)? diff_angle = 36000 - enkoders.Gyro_old + enkoders.Gyro: ((diff_angle < -7000)? diff_angle = -36000 + enkoders.Gyro - enkoders.Gyro_old: 0);
    location.fi = location.fi_old + diff_angle/100.0*M_PI/180.0;
    location.fi = fmod(location.fi + ((location.fi >= 0) ? 0 : 2*M_PI),2*M_PI);
}

void MainWindow::calculateAxis()
{
    long double l = calculateDrivenLength();
    location.x = location.x_old + l * cos(location.fi);
    location.y = location.y_old + l * sin(location.fi);
}

long double MainWindow::calculateDrivenLength()
{
    //https://electronics.stackexchange.com/questions/410350/how-to-handle-rotary-encoder-overflow  error = (setPoint + 512 - currPos) % 1024 - 512;
    signed short diff_r = ((enkoders.R - enkoders.R_old + 32768) % 65536 - 32768);    //(enkoders.R - enkoders.R_old);
    signed short diff_l = ((enkoders.L - enkoders.L_old + 32768) % 65536 - 32768);    //(enkoders.L - enkoders.L_old);

    long double l_rk = robot.getTickToMeter() * diff_r;
    long double l_lk = robot.getTickToMeter() * diff_l;

    return (l_rk + l_lk)/2.0;
}

void MainWindow::savePreviousValues()
{
    location.x_old = location.x;
    location.y_old = location.y;
    location.fi_old = location.fi;
    enkoders.R_old = enkoders.R;
    enkoders.L_old = enkoders.L;
    enkoders.Gyro_old = enkoders.Gyro;
}

//funkcia local robot je na priame riadenie robota, ktory je vo vasej blizskoti, viete si z dat ratat polohu atd (zapnutie dopravneho oneskorenia sposobi opozdenie dat oproti aktualnemu stavu robota)
void MainWindow::localrobot(TKobukiData &sens)
{
    if(prvyStart)
    {
        initEncodersValues();
        prvyStart=false;
    }
    enkoders.R = sens.EncoderRight;
    enkoders.L = sens.EncoderLeft;
    enkoders.Gyro = sens.GyroAngle + 17999;

    calculateActualAngle();
    calculateAxis();

    PomEncoderL=sens.EncoderLeft;


    ui->statusBar->showMessage(QString("X: %1,\t Y: %2,\t Fi: %3").arg((double)((int)(location.x*10000))/10000).arg((double)((int)(location.y*10000))/10000).arg((double)((int)((location.fi/M_PI*180 + (location.fi > M_PI ? -360 : 0))*100))/100));

    if(keyboard_control == 0 && safety_stop == 0){
        checkGesture();
    }
    savePreviousValues();

}

// funkcia local laser je naspracovanie dat z lasera(zapnutie dopravneho oneskorenia sposobi opozdenie dat oproti aktualnemu stavu robota)
int MainWindow::locallaser(LaserMeasurement &laserData)
{

    paintThisLidar(laserData);


    //priklad ako zastavit robot ak je nieco prilis blizko
//    if(laserData.Data[i].scanDistance<500)
//    {
//        sendRobotCommand(ROBOT_STOP);
//    }
    ///PASTE YOUR CODE HERE
    /// ****************
    /// mozne hodnoty v return
    /// ROBOT_VPRED
    /// ROBOT_VZAD
    /// ROBOT_VLAVO
    /// ROBOT_VPRAVO
    /// ROBOT_STOP
    /// ROBOT_ARC
    ///
    /// ****************

    return -1;
}


//--autonomousrobot simuluje slucku robota, ktora bezi priamo na robote
// predstavte si to tak,ze ste naprogramovali napriklad polohovy regulator, uploadli ste ho do robota a tam sa to vykonava
// dopravne oneskorenie nema vplyv na data
void MainWindow::autonomousrobot(TKobukiData &sens)
{
    ///PASTE YOUR CODE HERE
    /// ****************
    ///
    ///
    /// ****************
}
//--autonomouslaser simuluje spracovanie dat z robota, ktora bezi priamo na robote
// predstavte si to tak,ze ste naprogramovali napriklad sposob obchadzania prekazky, uploadli ste ho do robota a tam sa to vykonava
// dopravne oneskorenie nema vplyv na data
int MainWindow::autonomouslaser(LaserMeasurement &laserData)
{
    ///PASTE YOUR CODE HERE
    /// ****************
    ///
    ///
    /// ****************
    return -1;
}

///kamera nema svoju vlastnu funkciu ktora sa vola, ak chcete niekde pouzit obrazok, aktualny je v premennej
/// robotPicture alebo ekvivalent AutonomousrobotPicture
/// pozor na synchronizaciu, odporucam akonahle chcete robit nieco s obrazkom urobit si jeho lokalnu kopiu
/// cv::Mat frameBuf; robotPicture.copyTo(frameBuf);


//sposob kreslenia na obrazovku, tento event sa spusti vzdy ked sa bud zavola funkcia update() alebo operacny system si vyziada prekreslenie okna

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.setBrush(Qt::transparent);
    QPen pero;
    pero.setStyle(Qt::SolidLine);
    pero.setWidth(3);
    pero.setColor(Qt::white);
    QRect rect(20,160,600,400);
    int distances[] = {600, 500, 400, 300, 280, 225};
    rect = ui->frame->geometry();
    rect.translate(0,12);
    int warning_message = 0;

    double size = 0.2;
    size *= ((rect.height()>rect.width())? rect.height(): rect.width());



    if(updateCameraPicture==1 && showCamera==true){
        cv::Mat frameBuf;
        robotPicture.copyTo(frameBuf);

        for(int k=0; k < paintLaserData.numberOfScans; k++)
        {
            //cout<<"c:" <<k<< "  "<< paintLaserData.Data[k].scanDistance<<endl;
            //<<"uhol["<<k<<"] = "<< ((360.0-paintLaserData.Data[k].scanAngle)) << endl;

            double angle = (360.0 - paintLaserData.Data[k].scanAngle) * M_PI / 180.0;

            if(paintLaserData.Data[k].scanDistance < 10 || (angle > 54/2*M_PI/180 && angle < (360 - 54/2)*M_PI/180)){
                continue;
            }

            double dist = paintLaserData.Data[k].scanDistance/1000.0;

            double z = dist*cos(angle);
            double x = dist*sin(angle);

            int x_obr= frameBuf.cols/2 - f*x/z;
            int y_obr= frameBuf.rows/2 + f*0.06/z;
            //imgIn.setPixel(x_obr, y_obr, qRgb(0, 0, 0));
            if((x_obr >= 0 && x_obr <= (frameBuf.cols - 1)) && (y_obr >= 0 && y_obr <= (frameBuf.rows - 1))){
                cv::Vec3b & rgb = frameBuf.at<cv::Vec3b>(y_obr,x_obr);



                cv::Scalar colors[] ={cv::Scalar(0, 0, 255), cv::Scalar(0, 70, 255), cv::Scalar(0, 140, 255), cv::Scalar(0, 200, 255), cv::Scalar(0, 255, 255), cv::Scalar(0, 255, 240)};
                if(paintLaserData.Data[k].scanDistance < distances[3]){

                    cv::Rect ccomp;
                    cv::floodFill(frameBuf, cv::Point(x_obr, y_obr), colors[0], &ccomp, cv::Scalar(25, 25, 25),
                                  cv::Scalar(25, 25, 25), 4);
                }else if(paintLaserData.Data[k].scanDistance < distances[2]){
                    cv::Vec3b & rgb = frameBuf.at<cv::Vec3b>(y_obr,x_obr);
                    cv::Scalar color = (rgb[0], rgb[1], rgb[2]);
                    if(color == colors[0]){
                        continue;
                    }

                    cv::Rect ccomp;
                    cv::floodFill(frameBuf, cv::Point(x_obr, y_obr), colors[1], &ccomp, cv::Scalar(25, 25, 25),
                                  cv::Scalar(25, 25, 25), 4);
                }else if(paintLaserData.Data[k].scanDistance < distances[1]){
                    cv::Vec3b & rgb = frameBuf.at<cv::Vec3b>(y_obr,x_obr);
                    cv::Scalar color = (rgb[0], rgb[1], rgb[2]);
                    if(color == colors[0] || color == colors[1]){
                        continue;
                    }
                    cv::Rect ccomp;
                    cv::floodFill(frameBuf, cv::Point(x_obr, y_obr), colors[2], &ccomp, cv::Scalar(25, 25, 25),
                                  cv::Scalar(25, 25, 25), 4);
                }else if(paintLaserData.Data[k].scanDistance < distances[0]){
                    //
                    cv::Vec3b & rgb = frameBuf.at<cv::Vec3b>(y_obr,x_obr);
                    cv::Scalar color = (rgb[0], rgb[1], rgb[2]);
                    if(color == colors[0] || color == colors[1] || color == colors[2]){
                        continue;
                    }
                    cv::Rect ccomp;
                    cv::floodFill(frameBuf, cv::Point(x_obr, y_obr), colors[3], &ccomp, cv::Scalar(25, 25, 25),
                                  cv::Scalar(25, 25, 25), 4);
                }else if(dist < 1){
                    cv::Vec3b & rgb = frameBuf.at<cv::Vec3b>(y_obr,x_obr);
                    cv::Scalar color = (rgb[0], rgb[1], rgb[2]);
                    if(color == colors[0] || color == colors[1] || color == colors[2]|| color == colors[3]){
                        continue;
                    }
                    cv::Rect ccomp;
                    cv::floodFill(frameBuf, cv::Point(x_obr, y_obr), colors[4], &ccomp, cv::Scalar(25, 25, 25),
                                  cv::Scalar(25, 25, 25), 4);
                }else if(dist < 2){
                    cv::Vec3b & rgb = frameBuf.at<cv::Vec3b>(y_obr,x_obr);
                    cv::Scalar color = (rgb[0], rgb[1], rgb[2]);
                    if(color == colors[0] || color == colors[1] || color == colors[2] || color == colors[3] || color == colors[4]){
                        continue;
                    }
                    cv::Rect ccomp;
                    cv::floodFill(frameBuf, cv::Point(x_obr, y_obr), colors[5], &ccomp, cv::Scalar(25, 25, 25),
                                  cv::Scalar(25, 25, 25), 4);
                }
            }
        }





        updateCameraPicture=0;
        QImage imgIn= QImage((uchar*) frameBuf.data, frameBuf.cols, frameBuf.rows, frameBuf.step, QImage::Format_BGR888);// robotPicture.cols, robotPicture.rows, robotPicture.step, QImage::Format_BGR888);//robotPicture.data

        painter.drawImage(rect, imgIn);

    }else
    {
        QImage imgIn= QImage((uchar*) robotPicture.data, robotPicture.cols, robotPicture.rows, robotPicture.step, QImage::Format_BGR888);
        painter.drawImage(rect,imgIn);
    }

    if(updateLaserPicture==1 && showLidar==true)
    {


        /// ****************
        ///you can change pen or pen color here if you want
        /// ****************

        pero.setWidth(3);
        pero.setColor(Qt::white);
        painter.setPen(pero);

        QRect rect1(rect.topLeft().x() , rect.topLeft().y(), size, size);

        double warnings[] = {5000,5000,5000,5000,5000,5000,5000,5000};
        double min_dist = 5000;
        for(int k=0;k<paintLaserData.numberOfScans;k++)
        {
            if(paintLaserData.Data[k].scanDistance < 10){
                continue;
            }
            if(min_dist > paintLaserData.Data[k].scanDistance){
                min_dist = paintLaserData.Data[k].scanDistance;
            }

            int dist=paintLaserData.Data[k].scanDistance/35;

            int xp=rect1.width()-(rect1.width()/2+dist*2*sin((-90+360.0-paintLaserData.Data[k].scanAngle)*3.14159/180.0))+rect1.topLeft().x();
            int yp=rect1.height()-(rect1.height()/2+dist*2*cos((-90+360.0-paintLaserData.Data[k].scanAngle)*3.14159/180.0))+rect1.topLeft().y();

            //podmienka aby body nepretekali mimo rect
            if(rect1.contains(xp-4,yp-4) && rect1.contains(xp+4,yp+4)){
                pero.setColor(QColor(0,0,0,255));
                pero.setWidth(8);
                painter.setPen(pero);
                painter.drawEllipse(QPoint(xp, yp),1,1);
            }
        }

        for(int k=0;k<paintLaserData.numberOfScans;k++)
        {

            if(paintLaserData.Data[k].scanDistance < 10){
                continue;
            }

            switch(((int)((360-paintLaserData.Data[k].scanAngle)/45)+2)%8){
                case 0:

                    warnings[0] = (warnings[0] < paintLaserData.Data[k].scanDistance) ? warnings[0] : paintLaserData.Data[k].scanDistance;

                    break;
                case 1:
                    warnings[1] = (warnings[1] < paintLaserData.Data[k].scanDistance) ? warnings[1] : paintLaserData.Data[k].scanDistance;
                    break;
                case 2:
                    warnings[2] = (warnings[2] < paintLaserData.Data[k].scanDistance) ? warnings[2] : paintLaserData.Data[k].scanDistance;
                    break;
                case 3:
                    warnings[3] = (warnings[3] < paintLaserData.Data[k].scanDistance) ? warnings[3] : paintLaserData.Data[k].scanDistance;
                    break;
                case 4:

                    warnings[4] = (warnings[4] < paintLaserData.Data[k].scanDistance) ? warnings[4] : paintLaserData.Data[k].scanDistance;
                    break;
                case 5:
                    warnings[5] = (warnings[5] < paintLaserData.Data[k].scanDistance) ? warnings[5] : paintLaserData.Data[k].scanDistance;
                    break;
                case 6:
                    warnings[6] = (warnings[6] < paintLaserData.Data[k].scanDistance) ? warnings[6] : paintLaserData.Data[k].scanDistance;
                    break;
                case 7:
                    warnings[7] = (warnings[7] < paintLaserData.Data[k].scanDistance) ? warnings[7] : paintLaserData.Data[k].scanDistance;
                    break;
                }


            int dist=paintLaserData.Data[k].scanDistance/35;

            int xp=rect1.width()-(rect1.width()/2+dist*2*sin((-90+360.0-paintLaserData.Data[k].scanAngle)*3.14159/180.0))+rect1.topLeft().x();
            int yp=rect1.height()-(rect1.height()/2+dist*2*cos((-90+360.0-paintLaserData.Data[k].scanAngle)*3.14159/180.0))+rect1.topLeft().y();


            if(rect1.contains(xp-4,yp-4)&& rect1.contains(xp+4,yp+4)){

                pero.setColor(QColor(0,0,0,5));
                pero.setWidth(6);
                painter.setPen(pero);
                painter.drawEllipse(QPoint(xp, yp),1,1);
                if(paintLaserData.Data[k].scanDistance >= distances[0]){
                    pero.setColor(Qt::white);
                    pero.setWidth(4);
                    painter.setPen(pero);
                    painter.drawEllipse(QPoint(xp, yp),1,1);
                    pero.setWidth(2);
                    painter.setPen(pero);
                    painter.drawEllipse(QPoint(xp, yp),1,1);
                }else if(paintLaserData.Data[k].scanDistance >= distances[1]){
                    pero.setColor(QColor(255,200,0));
                    pero.setWidth(4);
                    painter.setPen(pero);
                    painter.drawEllipse(QPoint(xp, yp),1,1);
                    pero.setWidth(2);
                    painter.setPen(pero);
                    painter.drawEllipse(QPoint(xp, yp),1,1);
                }else if(paintLaserData.Data[k].scanDistance >= distances[2]){
                    pero.setColor(QColor(255,140,0));
                    pero.setWidth(4);
                    painter.setPen(pero);
                    painter.drawEllipse(QPoint(xp, yp),1,1);
                    pero.setWidth(2);
                    painter.setPen(pero);
                    painter.drawEllipse(QPoint(xp, yp),1,1);
                }else if(paintLaserData.Data[k].scanDistance >= distances[3]){
                    pero.setColor(QColor(255,70,0));
                    pero.setWidth(4);
                    painter.setPen(pero);
                    painter.drawEllipse(QPoint(xp, yp),1,1);
                    pero.setWidth(2);
                    painter.setPen(pero);
                    painter.drawEllipse(QPoint(xp, yp),1,1);
                }else{
                    pero.setColor(QColor(255,0,0));
                    pero.setWidth(4);
                    painter.setPen(pero);
                    painter.drawEllipse(QPoint(xp, yp),1,1);
                    pero.setWidth(2);
                    painter.setPen(pero);
                    painter.drawEllipse(QPoint(xp, yp),1,1);
                }

                if(paintLaserData.Data[k].scanDistance < distances[4] && paintLaserData.Data[k].scanDistance >= distances[5] && warning_message != 2){
                    warning_message = 1;
                }else if(paintLaserData.Data[k].scanDistance < distances[5]){
                    warning_message = 2;
                }

            }

        }

        for(int i = 0; i < 8; i++){
            if(warnings[i] < distances[3]){
                warnings[i] = 4;
            }else if(warnings[i] < distances[2]){
                warnings[i] = 3;
            }else if(warnings[i] < distances[1]){
                warnings[i] = 2;
            }else if(warnings[i] < distances[0]){
                warnings[i] = 1;
            }else {
                warnings[i] = 0;
            }
        }

        //vykreslenie robota
        pero.setColor(Qt::black);
        pero.setWidth(4);
        painter.setPen(pero);
        int xp=rect1.topLeft().x()+(rect1.width()/2);
        int yp=rect1.topLeft().y()+(rect1.height()/2);
        if(rect1.contains(xp,yp)){

            painter.drawEllipse(QPoint(xp, yp), 7, 7);
            painter.drawLine(QPoint(xp, yp),QPoint(xp+4, yp));
            pero.setColor(Qt::white);
            pero.setWidth(2);
            painter.setPen(pero);
            painter.drawEllipse(QPoint(xp, yp), 7, 7);
            painter.drawLine(QPoint(xp, yp),QPoint(xp+6, yp));
        }





        QRectF rect3(rect.bottomLeft().x() + (rect.width() - 128)/2, rect.bottomLeft().y() - 128, 128, 128);
        painter.fillRect(rect1, QColor(0, 0, 0, 100));//QBrush(Qt::black, Qt::SolidPattern)
        painter.fillRect(rect3, QColor(0, 0, 0, 0));

        //vykreslenie robota
        int x_p = rect3.topLeft().x() + rect3.width() / 2;
        int y_p = rect3.bottomLeft().y() - 128 / 2;
        QPoint middle = QPoint(x_p, y_p);
        pero.setWidth(64);
        pero.setColor(QColor(0, 0, 0, 120));
        painter.setPen(pero);
        painter.drawEllipse(middle, 32, 32);


        pero.setWidth(6);
        pero.setColor(Qt::black);
        painter.setPen(pero);

        painter.drawEllipse(middle, 25, 25);
        painter.drawLine(middle, QPoint(x_p, y_p - 25));
        pero.setWidth(3);
        pero.setColor(Qt::white);
        painter.setPen(pero);

        painter.drawEllipse(middle, 25, 25);
        painter.drawLine(middle, QPoint(x_p, y_p - 25));

        pero.setColor(QColor(0, 0, 0, 20));
        pero.setWidth(6);
        painter.setPen(pero);

        int diameter = 70;
        for(int i = 0; i < 8; i++){
            painter.drawArc(x_p - diameter / 2, y_p - diameter / 2, diameter, diameter, (i * 45 + 10) * 16, 25 * 16);
        }

        diameter = 85;
        for(int i = 0; i < 8; i++){
            painter.drawArc(x_p - diameter / 2, y_p - diameter / 2, diameter, diameter, (i * 45+ 10) * 16, 25 * 16);
        }

        diameter = 100;
        for(int i = 0; i < 8; i++){
            painter.drawArc(x_p - diameter / 2, y_p - diameter / 2, diameter, diameter, (i * 45+ 10) * 16, 25 * 16);
        }

        diameter = 115;
        for(int i = 0; i < 8; i++){
            painter.drawArc(x_p - diameter / 2, y_p - diameter / 2, diameter, diameter, (i * 45+ 10) * 16, 25 * 16);
        }

        pero.setColor(Qt::black);
        pero.setWidth(6);
        painter.setPen(pero);

        diameter = 70;
        for(int i = 0; i < 8; i++){
            if(warnings[i] > 0){
                painter.drawArc(x_p - diameter / 2, y_p - diameter / 2, diameter, diameter, (i * 45 + 10) * 16, 25 * 16);
            }
        }

        diameter = 85;
        for(int i = 0; i < 8; i++){
            if(warnings[i] > 1){
                painter.drawArc(x_p - diameter / 2, y_p - diameter / 2, diameter, diameter, (i * 45 + 7) * 16, 31 * 16);
            }
        }

        diameter = 100;
        for(int i = 0; i < 8; i++){
            if(warnings[i] > 2){
                painter.drawArc(x_p - diameter / 2, y_p - diameter / 2, diameter, diameter, (i * 45 + 6.5) * 16, 32 * 16);
            }
        }

        diameter = 115;
        for(int i = 0; i < 8; i++){
            if(warnings[i] > 3){
                painter.drawArc(x_p - diameter / 2, y_p - diameter / 2, diameter, diameter, (i * 45 + 6) * 16, 33 * 16);
            }
        }


        pero.setColor(QColor(255,200,0));
        pero.setWidth(3);
        painter.setPen(pero);
        diameter = 70;
        for(int i = 0; i < 8; i++){
            if(warnings[i] > 0){
                painter.drawArc(x_p - diameter / 2, y_p - diameter / 2, diameter, diameter, (i * 45 + 10) * 16, 25 * 16);
            }
        }

        pero.setColor(QColor(255,140,0));
        painter.setPen(pero);
        diameter = 85;
        for(int i = 0; i < 8; i++){
            if(warnings[i] > 1){
                painter.drawArc(x_p - diameter / 2, y_p - diameter / 2, diameter, diameter, (i*45+7)*16, 31*16);
            }
        }

        pero.setColor(QColor(255,70,0));
        painter.setPen(pero);
        diameter = 100;
        for(int i = 0; i < 8; i++){
            if(warnings[i] > 2){
                painter.drawArc(x_p-diameter / 2, y_p - diameter / 2, diameter, diameter, (i * 45 + 6.5) * 16, 32 * 16);
            }
        }

        pero.setColor(QColor(255,0,0));
        painter.setPen(pero);
        diameter = 115;
        for(int i = 0; i < 8; i++){
            if(warnings[i] > 3){
                painter.drawArc(x_p-diameter/2, y_p - diameter / 2, diameter, diameter, (i * 45 + 6) * 16, 33 * 16);
            }
        }
    }
    if(updateSkeletonPicture==1 && showSkeleton==true)
    {
        double size = 0.2;
        size *= ((rect.height()>rect.width())? rect.height(): rect.width());

        QRect rect2(rect.topRight().x() - size, rect.topRight().y(), size, size);


        painter.fillRect(rect2, QColor(0,0,0,100));
        pero.setColor(Qt::white);
        pero.setWidth(2);
        painter.setPen(pero);


        for(int i = 0; i < 42; i++)
        {
            int xp = rect2.topRight().x() -  size * kostricka.joints[i].x;
            int yp = rect2.topRight().y() +  size * kostricka.joints[i].y;
            if(rect2.contains(xp, yp)){
                painter.drawEllipse(QPoint(xp, yp), 0, 0);
                painter.drawPoint(xp, yp);
            }
        }
    }
    if(pohyb){
        int font_size = 16;
        QRect rect5(rect.center().x() - size*1.5/2, rect.topLeft().y(), 1.5*size, 1.5*font_size);

        painter.fillRect(rect5, QColor(0, 0, 0, 100));

        pero.setColor(Qt::white);
        pero.setWidth(4);
        painter.setFont(QFont("Arial", font_size, QFont::Bold));
        painter.setPen(pero);
        if(pohyb == 8){
            painter.drawText(rect5, Qt::AlignCenter, tr("Pohyb vpred"));
        }else if(pohyb == 2){
            painter.drawText(rect5, Qt::AlignCenter, tr("Pohyb vzad"));
        }else if(pohyb == 4){
            painter.drawText(rect5, Qt::AlignCenter, tr("Pohyb vlavo"));
        }else if(pohyb == 6){
            painter.drawText(rect5, Qt::AlignCenter, tr("Pohyb vpravo"));
        }

    }

    if(safety_stop){
        int font_size = 20;
        QRect rect5(rect.center().x() - size, rect.topLeft().y(), 2*size, 1.5*font_size);
        painter.fillRect(rect5, QColor(255,255,255));
        pero.setColor(Qt::red);
        painter.setPen(pero);
        painter.setFont(QFont("Arial", font_size, QFont::Bold));
        painter.drawText(rect5, Qt::AlignCenter, tr("SAFETY STOP"));
    }

    int font_size = 40;
    QRect rect5(rect.center().x() - 1.5*size, rect.center().y() - 1.5*font_size, 3*size, 3*font_size*size/125);

    pero.setColor(Qt::red);
    painter.setPen(pero);
    painter.setFont(QFont("Arial", font_size, QFont::Bold));
    if(warning_message == 1){
        painter.fillRect(rect5, QColor(255,255,255, 150));
        painter.drawText(rect5, Qt::AlignCenter, tr("Caution\nwall"));
    }else if(warning_message == 2){
        painter.fillRect(rect5, QColor(255,255,255, 200));
        painter.drawText(rect5, Qt::AlignCenter, tr("Collision"));
    }

}



///konstruktor aplikacie, nic co tu je nevymazavajte, ak potrebujete, mozete si tu pridat nejake inicializacne parametre
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    robotX=0;
    robotY=0;
    robotFi=0;

    showCamera=true;
    showLidar=true;
    showSkeleton=true;
    applyDelay=false;
    dl=0;
    stopall=1;
    prvyStart=true;
    updateCameraPicture=0;
    ipaddress="127.0.0.1";
    std::function<void(void)> f =std::bind(&robotUDPVlakno, (void *)this);
    robotthreadHandle=std::move(std::thread(f));
    std::function<void(void)> f2 =std::bind(&laserUDPVlakno, (void *)this);
    laserthreadHandle=std::move(std::thread(f2));


    std::function<void(void)> f3 =std::bind(&skeletonUDPVlakno, (void *)this);
    skeletonthreadHandle=std::move(std::thread(f3));

    //--ak by ste nahodou chceli konzolu do ktorej mozete vypisovat cez std::cout, odkomentujte nasledujuce dva riadky
   // AllocConsole();
   // freopen("CONOUT$", "w", stdout);


    QFuture<void> future = QtConcurrent::run([=]() {
        imageViewer();
        // Code in this block will run in another thread
    });

        Imager.start();
}

///funkcia co sa zavola ked stlacite klavesu na klavesnici..
/// pozor, ak niektory widget akceptuje klavesu, sem sa nemusite (ale mozete) dostat
/// zalezi na to ako konkretny widget spracuje svoj event

double MainWindow::distanceBetweenJoints(short firstJoint, short secondJoint){

    return sqrt(pow(kostricka.joints[secondJoint].x-kostricka.joints[firstJoint].x,2) + pow(kostricka.joints[secondJoint].y-kostricka.joints[firstJoint].y,2));
}

double MainWindow::angleBetweenJoints(short firstJoint, short secondJoint){
    return fmod(atan2(kostricka.joints[secondJoint].y - kostricka.joints[firstJoint].y, kostricka.joints[secondJoint].x - kostricka.joints[firstJoint].x)+2*M_PI,2.0*M_PI);
}

int MainWindow::checkGesture(){
    char tt;
    //Pohyb Vlavo
    if(abs(angleBetweenJoints(jointnames(right_wrist), jointnames(right_thumb_tip)) - angleBetweenJoints(jointnames(right_wrist), jointnames(right_index_cmc)))*180/M_PI >= 25){
        if(distanceBetweenJoints(jointnames(right_index_cmc), jointnames(right_thumb_tip)) > distanceBetweenJoints(jointnames(right_index_cmc), jointnames(right_thumb_ip))){
            if(distanceBetweenJoints(jointnames(right_wrist), jointnames(right_index_tip)) < distanceBetweenJoints(jointnames(right_wrist), jointnames(right_index_cmc))){
                if(distanceBetweenJoints(jointnames(right_wrist), jointnames(right_middle_tip)) < distanceBetweenJoints(jointnames(right_wrist), jointnames(right_middle_cmc))){
                    if(distanceBetweenJoints(jointnames(right_wrist), jointnames(right_ringy_tip)) < distanceBetweenJoints(jointnames(right_wrist), jointnames(right_ring_cmc))){
                        if(distanceBetweenJoints(jointnames(right_wrist), jointnames(right_pink_tip)) < distanceBetweenJoints(jointnames(right_wrist), jointnames(right_pinky_cmc))){

                            if(distanceBetweenJoints(jointnames(left_wrist), jointnames(left_index_tip)) < distanceBetweenJoints(jointnames(left_wrist), jointnames(left_index_cmc))){
                                if(distanceBetweenJoints(jointnames(left_wrist), jointnames(left_middle_tip)) < distanceBetweenJoints(jointnames(left_wrist), jointnames(left_middle_cmc))){
                                    if(distanceBetweenJoints(jointnames(left_wrist), jointnames(left_ringy_tip)) < distanceBetweenJoints(jointnames(left_wrist), jointnames(left_ring_cmc))){
                                        if(distanceBetweenJoints(jointnames(left_wrist), jointnames(left_pink_tip)) < distanceBetweenJoints(jointnames(left_wrist), jointnames(left_pinky_cmc))){
                                            if(abs(angleBetweenJoints(jointnames(left_wrist), jointnames(left_thumb_tip)) - angleBetweenJoints(jointnames(left_wrist), jointnames(left_index_cmc)))*180/M_PI < 12){
                                                tt = ROBOT_VLAVO;
                                                sendRobotCommand(tt,3.14159/4);
                                                pohyb = 4;
                                                return pohyb;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    //Pohyb Vpravo
    if(abs(angleBetweenJoints(jointnames(left_wrist), jointnames(left_thumb_tip)) - angleBetweenJoints(jointnames(left_wrist), jointnames(left_index_cmc)))*180/M_PI >= 25){
        if(distanceBetweenJoints(jointnames(left_index_cmc), jointnames(left_thumb_tip)) > distanceBetweenJoints(jointnames(left_index_cmc), jointnames(left_thumb_ip))){
            if(distanceBetweenJoints(jointnames(left_wrist), jointnames(left_index_tip)) < distanceBetweenJoints(jointnames(left_wrist), jointnames(left_index_cmc))){
                if(distanceBetweenJoints(jointnames(left_wrist), jointnames(left_middle_tip)) < distanceBetweenJoints(jointnames(left_wrist), jointnames(left_middle_cmc))){
                    if(distanceBetweenJoints(jointnames(left_wrist), jointnames(left_ringy_tip)) < distanceBetweenJoints(jointnames(left_wrist), jointnames(left_ring_cmc))){
                        if(distanceBetweenJoints(jointnames(left_wrist), jointnames(left_pink_tip)) < distanceBetweenJoints(jointnames(left_wrist), jointnames(left_pinky_cmc))){

                            if(distanceBetweenJoints(jointnames(right_wrist), jointnames(right_index_tip)) < distanceBetweenJoints(jointnames(right_wrist), jointnames(right_index_cmc))){
                                if(distanceBetweenJoints(jointnames(right_wrist), jointnames(right_middle_tip)) < distanceBetweenJoints(jointnames(right_wrist), jointnames(right_middle_cmc))){
                                    if(distanceBetweenJoints(jointnames(right_wrist), jointnames(right_ringy_tip)) < distanceBetweenJoints(jointnames(right_wrist), jointnames(right_ring_cmc))){
                                        if(distanceBetweenJoints(jointnames(right_wrist), jointnames(right_pink_tip)) < distanceBetweenJoints(jointnames(right_wrist), jointnames(right_pinky_cmc))){
                                            if(abs(angleBetweenJoints(jointnames(right_wrist), jointnames(right_thumb_tip)) - angleBetweenJoints(jointnames(right_wrist), jointnames(right_index_cmc)))*180/M_PI < 12){
                                                tt = ROBOT_VPRAVO;
                                                sendRobotCommand(tt,-3.14159/4);
                                                pohyb = 6;
                                                return pohyb;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    // Pohyb vpred
    if(distanceBetweenJoints(jointnames(left_index_tip), jointnames(left_wrist)) > distanceBetweenJoints(jointnames(left_index_ip), jointnames(left_wrist))){
        if(distanceBetweenJoints(jointnames(right_index_tip), jointnames(right_wrist)) > distanceBetweenJoints(jointnames(right_index_ip), jointnames(right_wrist))){
            if(distanceBetweenJoints(jointnames(left_index_ip), jointnames(left_wrist)) > distanceBetweenJoints(jointnames(left_index_mcp), jointnames(left_wrist))){
                if(distanceBetweenJoints(jointnames(right_index_ip), jointnames(right_wrist)) > distanceBetweenJoints(jointnames(right_index_mcp), jointnames(right_wrist))){
                    if(distanceBetweenJoints(jointnames(left_index_mcp), jointnames(left_wrist)) > distanceBetweenJoints(jointnames(left_index_cmc), jointnames(left_wrist))){
                        if(distanceBetweenJoints(jointnames(right_index_mcp), jointnames(right_wrist)) > distanceBetweenJoints(jointnames(right_index_cmc), jointnames(right_wrist))){
                            if(distanceBetweenJoints(jointnames(left_index_mcp), jointnames(left_index_ip)) < distanceBetweenJoints(jointnames(left_index_mcp), jointnames(left_middle_mcp))){
                                if(distanceBetweenJoints(jointnames(right_index_mcp), jointnames(right_index_ip)) < distanceBetweenJoints(jointnames(right_index_mcp), jointnames(right_middle_mcp))){

                                    if(distanceBetweenJoints(jointnames(right_wrist), jointnames(right_middle_tip)) < distanceBetweenJoints(jointnames(right_wrist), jointnames(right_middle_cmc))){
                                        if(distanceBetweenJoints(jointnames(right_wrist), jointnames(right_ringy_tip)) < distanceBetweenJoints(jointnames(right_wrist), jointnames(right_ring_cmc))){
                                            if(distanceBetweenJoints(jointnames(right_wrist), jointnames(right_pink_tip)) < distanceBetweenJoints(jointnames(right_wrist), jointnames(right_pinky_cmc))){
                                                if(abs(angleBetweenJoints(jointnames(right_wrist), jointnames(right_thumb_tip)) - angleBetweenJoints(jointnames(right_wrist), jointnames(right_index_cmc)))*180/M_PI < 12){

                                                    if(distanceBetweenJoints(jointnames(left_wrist), jointnames(left_middle_tip)) < distanceBetweenJoints(jointnames(left_wrist), jointnames(left_middle_cmc))){
                                                        if(distanceBetweenJoints(jointnames(left_wrist), jointnames(left_ringy_tip)) < distanceBetweenJoints(jointnames(left_wrist), jointnames(left_ring_cmc))){
                                                            if(distanceBetweenJoints(jointnames(left_wrist), jointnames(left_pink_tip)) < distanceBetweenJoints(jointnames(left_wrist), jointnames(left_pinky_cmc))){
                                                                if(abs(angleBetweenJoints(jointnames(left_wrist), jointnames(left_thumb_tip)) - angleBetweenJoints(jointnames(left_wrist), jointnames(left_index_cmc)))*180/M_PI < 12){
                                                                    tt = ROBOT_VPRED;
                                                                    sendRobotCommand(tt,250);
                                                                    pohyb = 8;
                                                                    return pohyb;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }
    }
    // Pohyb vzad
    if(distanceBetweenJoints(jointnames(left_pink_tip), jointnames(left_wrist)) > distanceBetweenJoints(jointnames(left_pink_ip), jointnames(left_wrist))){
        if(distanceBetweenJoints(jointnames(right_pink_tip), jointnames(right_wrist)) > distanceBetweenJoints(jointnames(right_pink_ip), jointnames(right_wrist))){
            if(distanceBetweenJoints(jointnames(left_pink_ip), jointnames(left_wrist)) > distanceBetweenJoints(jointnames(left_pink_mcp), jointnames(left_wrist))){
                if(distanceBetweenJoints(jointnames(right_pink_ip), jointnames(right_wrist)) > distanceBetweenJoints(jointnames(right_pink_mcp), jointnames(right_wrist))){
                    if(distanceBetweenJoints(jointnames(left_pink_mcp), jointnames(left_wrist)) > distanceBetweenJoints(jointnames(left_pinky_cmc), jointnames(left_wrist))){
                        if(distanceBetweenJoints(jointnames(right_pink_mcp), jointnames(right_wrist)) > distanceBetweenJoints(jointnames(right_pinky_cmc), jointnames(right_wrist))){
                            if(distanceBetweenJoints(jointnames(left_pink_mcp), jointnames(left_pink_ip)) < distanceBetweenJoints(jointnames(left_pink_mcp), jointnames(left_middle_mcp))){
                                if(distanceBetweenJoints(jointnames(right_pink_mcp), jointnames(right_pink_ip)) < distanceBetweenJoints(jointnames(right_pink_mcp), jointnames(right_middle_mcp))){

                                    if(distanceBetweenJoints(jointnames(right_wrist), jointnames(right_middle_tip)) < distanceBetweenJoints(jointnames(right_wrist), jointnames(right_middle_cmc))){
                                        if(distanceBetweenJoints(jointnames(right_wrist), jointnames(right_ringy_tip)) < distanceBetweenJoints(jointnames(right_wrist), jointnames(right_ring_cmc))){
                                            if(distanceBetweenJoints(jointnames(right_wrist), jointnames(right_index_tip)) < distanceBetweenJoints(jointnames(right_wrist), jointnames(right_index_cmc))){
                                                if(abs(angleBetweenJoints(jointnames(right_wrist), jointnames(right_thumb_tip)) - angleBetweenJoints(jointnames(right_wrist), jointnames(right_index_cmc)))*180/M_PI < 12){

                                                    if(distanceBetweenJoints(jointnames(left_wrist), jointnames(left_middle_tip)) < distanceBetweenJoints(jointnames(left_wrist), jointnames(left_middle_cmc))){
                                                        if(distanceBetweenJoints(jointnames(left_wrist), jointnames(left_ringy_tip)) < distanceBetweenJoints(jointnames(left_wrist), jointnames(left_ring_cmc))){
                                                            if(distanceBetweenJoints(jointnames(left_wrist), jointnames(left_index_tip)) < distanceBetweenJoints(jointnames(left_wrist), jointnames(left_index_cmc))){
                                                                if(abs(angleBetweenJoints(jointnames(left_wrist), jointnames(left_thumb_tip)) - angleBetweenJoints(jointnames(left_wrist), jointnames(left_index_cmc)))*180/M_PI < 12){
                                                                    tt = ROBOT_VZAD;
                                                                    sendRobotCommand(tt,-250);
                                                                    pohyb = 2;
                                                                    return pohyb;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }
    }

    tt=0x01;
    sendRobotCommand(tt,0);
    pohyb = 0;
    return pohyb;
}

void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
    char tt = ROBOT_STOP;

    switch (event->key()) {


        //forward
        case Qt::Key_8:
            if(pohyb == 8){
                pohyb = 0;
                keyboard_control = 0;
                sendRobotCommand(tt);
            }

            break;
        case Qt::Key_W:
            if(pohyb == 8){
                pohyb = 0;
                keyboard_control = 0;
                sendRobotCommand(tt);
            }
            break;

        //backward
        case Qt::Key_2:
            if(pohyb == 2){
                pohyb = 0;
                keyboard_control = 0;
                sendRobotCommand(tt);
            }
            break;
        case Qt::Key_S:
            if(pohyb == 2){
                pohyb = 0;
                keyboard_control = 0;
                sendRobotCommand(tt);
            }
            break;

        //right
        case Qt::Key_6:
            if(pohyb == 6){
                pohyb = 0;
                keyboard_control = 0;
                sendRobotCommand(tt);
            }
            break;
        case Qt::Key_D:
            if(pohyb == 6){
                pohyb = 0;
                keyboard_control = 0;
                sendRobotCommand(tt);
            }
            break;

        //left
        case Qt::Key_4:
            if(pohyb == 4){
                pohyb = 0;
                keyboard_control = 0;
                sendRobotCommand(tt);
            }
            break;
        case Qt::Key_A:
            if(pohyb == 4){
                pohyb = 0;
                keyboard_control = 0;
                sendRobotCommand(tt);
            }
            break;
    }
}

void MainWindow::keyPressEvent(QKeyEvent* event)
{
    if(safety_stop){
        return;
    }

    char tt;

    switch (event->key()) {
        case 0x01000020:
            pohyb = 0;
            tt = ROBOT_STOP;
            cout<<"stop"<<endl;
            sendRobotCommand(tt);
        break;

        case Qt::Key_5:
            pohyb = 0;
            tt = ROBOT_STOP;
            sendRobotCommand(tt);
            break;

        //forward
        case Qt::Key_8:
            tt = ROBOT_VPRED;
            pohyb = 8;
            keyboard_control = 1;
            sendRobotCommand(tt,250);
            break;
        case Qt::Key_W:
            tt = ROBOT_VPRED;
            pohyb = 8;
            keyboard_control = 1;
            sendRobotCommand(tt,250);
            break;

        //backward
        case Qt::Key_2:
            tt = ROBOT_VZAD;
            pohyb = 2;
            keyboard_control = 1;
            sendRobotCommand(tt,-250);
            break;
        case Qt::Key_S:
            tt = ROBOT_VZAD;
            pohyb = 2;
            keyboard_control = 1;
            sendRobotCommand(tt,-250);
            break;

        //right
        case Qt::Key_6:
            tt = ROBOT_VPRAVO;
            pohyb = 6;
            keyboard_control = 1;
            sendRobotCommand(tt,-3.14159/4);
            break;
        case Qt::Key_D:
            tt = ROBOT_VPRAVO;
            pohyb = 6;
            keyboard_control = 1;
            sendRobotCommand(tt,-3.14159/4);
            break;

        //left
        case Qt::Key_4:
            tt = ROBOT_VLAVO;
            pohyb = 4;
            keyboard_control = 1;
            sendRobotCommand(tt,3.14159/4);
            break;
        case Qt::Key_A:
            tt = ROBOT_VLAVO;
            pohyb = 4;
            keyboard_control = 1;
            sendRobotCommand(tt,3.14159/4);
            break;
    }
}
//--cokolvek za tymto vas teoreticky nemusi zaujimat, su tam len nejake skarede kody





























































































































































































MainWindow::~MainWindow()
{
    stopall=0;
    laserthreadHandle.join();
    robotthreadHandle.join();
    skeletonthreadHandle.join();
    delete ui;
}









void MainWindow::robotprocess()
{
#ifdef _WIN32
    WSADATA wsaData = {0};
    int iResult = 0;
    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
#else
#endif
    rob_slen = sizeof(las_si_other);
    if ((rob_s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {

    }

    char rob_broadcastene=1;
    DWORD timeout=100;

    setsockopt(rob_s, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof timeout);
    setsockopt(rob_s,SOL_SOCKET,SO_BROADCAST,&rob_broadcastene,sizeof(rob_broadcastene));
    // zero out the structure
    memset((char *) &rob_si_me, 0, sizeof(rob_si_me));

    rob_si_me.sin_family = AF_INET;
    rob_si_me.sin_port = htons(53000);
    rob_si_me.sin_addr.s_addr = htonl(INADDR_ANY);

    rob_si_posli.sin_family = AF_INET;
    rob_si_posli.sin_port = htons(5300);
    rob_si_posli.sin_addr.s_addr =inet_addr(ipaddress.data());//inet_addr("10.0.0.1");// htonl(INADDR_BROADCAST);
    rob_slen = sizeof(rob_si_me);
    bind(rob_s , (struct sockaddr*)&rob_si_me, sizeof(rob_si_me) );

    std::vector<unsigned char> mess=robot.setDefaultPID();
    if (sendto(rob_s, (char*)mess.data(), sizeof(char)*mess.size(), 0, (struct sockaddr*) &rob_si_posli, rob_slen) == -1)
    {

    }
#ifdef _WIN32
    Sleep(100);
#else
    usleep(100*1000);
#endif
    mess=robot.setSound(440,1000);
    if (sendto(rob_s, (char*)mess.data(), sizeof(char)*mess.size(), 0, (struct sockaddr*) &rob_si_posli, rob_slen) == -1)
    {

    }
    unsigned char buff[50000];
    while(stopall==1)
    {

        memset(buff,0,50000*sizeof(char));
        if ((rob_recv_len = recvfrom(rob_s, (char*)&buff, sizeof(char)*50000, 0, (struct sockaddr *) &rob_si_other, &rob_slen)) == -1)
        {

            continue;
        }
        //tu mame data..zavolame si funkciu

        //     memcpy(&sens,buff,sizeof(sens));
        struct timespec t;
        //      clock_gettime(CLOCK_REALTIME,&t);

        int returnval=robot.fillData(sens,(unsigned char*)buff);
        if(returnval==0)
        {
            //     memcpy(&sens,buff,sizeof(sens));

            std::chrono::steady_clock::time_point timestampf=std::chrono::steady_clock::now();

            autonomousrobot(sens);

            if(applyDelay==true)
            {
                struct timespec t;
                RobotData newcommand;
                newcommand.sens=sens;
                //    memcpy(&newcommand.sens,&sens,sizeof(TKobukiData));
                //        clock_gettime(CLOCK_REALTIME,&t);
                newcommand.timestamp=std::chrono::steady_clock::now();;//(int64_t)(t.tv_sec) * (int64_t)1000000000 + (int64_t)(t.tv_nsec);
                auto timestamp=std::chrono::steady_clock::now();;//(int64_t)(t.tv_sec) * (int64_t)1000000000 + (int64_t)(t.tv_nsec);
                sensorQuerry.push_back(newcommand);
                for(int i=0;i<sensorQuerry.size();i++)
                {
                    if(( std::chrono::duration_cast<std::chrono::nanoseconds>(timestampf-sensorQuerry[i].timestamp)).count()>(2.5*1000000000))
                    {
                        localrobot(sensorQuerry[i].sens);
                        sensorQuerry.erase(sensorQuerry.begin()+i);
                        i--;
                        break;

                    }
                }

            }
            else
            {
                sensorQuerry.clear();
                localrobot(sens);
            }
        }


    }

    std::cout<<"koniec thread2"<<std::endl;
}
/// vravel som ze vas to nemusi zaujimat. tu nic nieje
/// nosy litlle bastard
void MainWindow::laserprocess()
{
#ifdef _WIN32
    WSADATA wsaData = {0};
    int iResult = 0;
    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
#else
#endif
    las_slen = sizeof(las_si_other);
    if ((las_s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {

    }

    char las_broadcastene=1;
#ifdef _WIN32
    DWORD timeout=100;

    setsockopt(las_s, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof timeout);
    setsockopt(las_s,SOL_SOCKET,SO_BROADCAST,&las_broadcastene,sizeof(las_broadcastene));
#else
    setsockopt(las_s,SOL_SOCKET,SO_BROADCAST,&las_broadcastene,sizeof(las_broadcastene));
#endif
    // zero out the structure
    memset((char *) &las_si_me, 0, sizeof(las_si_me));

    las_si_me.sin_family = AF_INET;
    las_si_me.sin_port = htons(52999);
    las_si_me.sin_addr.s_addr = htonl(INADDR_ANY);

    las_si_posli.sin_family = AF_INET;
    las_si_posli.sin_port = htons(5299);
    las_si_posli.sin_addr.s_addr = inet_addr(ipaddress.data());;//htonl(INADDR_BROADCAST);
    bind(las_s , (struct sockaddr*)&las_si_me, sizeof(las_si_me) );
    char command=0x00;
    if (sendto(las_s, &command, sizeof(command), 0, (struct sockaddr*) &las_si_posli, rob_slen) == -1)
    {

    }
    LaserMeasurement measure;
    while(stopall==1)
    {

        if ((las_recv_len = recvfrom(las_s, (char *)&measure.Data, sizeof(LaserData)*1000, 0, (struct sockaddr *) &las_si_other, &las_slen)) == -1)
        {

            continue;
        }
        measure.numberOfScans=las_recv_len/sizeof(LaserData);
        //tu mame data..zavolame si funkciu

        //     memcpy(&sens,buff,sizeof(sens));
        int returnValue=autonomouslaser(measure);

        if(applyDelay==true)
        {
            struct timespec t;
            LidarVector newcommand;
            memcpy(&newcommand.data,&measure,sizeof(LaserMeasurement));
            //    clock_gettime(CLOCK_REALTIME,&t);
            newcommand.timestamp=std::chrono::steady_clock::now();//(int64_t)(t.tv_sec) * (int64_t)1000000000 + (int64_t)(t.tv_nsec);
            auto timestamp=std::chrono::steady_clock::now();//(int64_t)(t.tv_sec) * (int64_t)1000000000 + (int64_t)(t.tv_nsec);
            lidarQuerry.push_back(newcommand);
            for(int i=0;i<lidarQuerry.size();i++)
            {
                if((std::chrono::duration_cast<std::chrono::nanoseconds>(timestamp-lidarQuerry[i].timestamp)).count()>(2.5*1000000000))
                {
                    returnValue=locallaser(lidarQuerry[i].data);
                    if(returnValue!=-1)
                    {
                        //sendRobotCommand(returnValue);
                    }
                    lidarQuerry.erase(lidarQuerry.begin()+i);
                    i--;
                    break;

                }
            }

        }
        else
        {


            returnValue=locallaser(measure);
            if(returnValue!=-1)
            {
                //sendRobotCommand(returnValue);
            }
        }
    }
    std::cout<<"koniec thread"<<std::endl;
}

void MainWindow::on_pushButton_7_clicked()
{


}

void MainWindow::sendRobotCommand(char command,double speed,int radius)
{
    globalcommand=command;
 //   if(applyDelay==false)
    {

        std::vector<unsigned char> mess;
        switch(command)
        {
        case  ROBOT_VPRED:
            mess=robot.setTranslationSpeed(speed);
            break;
        case ROBOT_VZAD:
            mess=robot.setTranslationSpeed(speed);
            break;
        case ROBOT_VLAVO:
            mess=robot.setRotationSpeed(speed);
            break;
        case ROBOT_VPRAVO:
            mess=robot.setRotationSpeed(speed);
            break;
        case ROBOT_STOP:
            mess=robot.setTranslationSpeed(0);
            break;
        case ROBOT_ARC:
            mess=robot.setArcSpeed(speed,radius);
            break;


        }
        if (sendto(rob_s, (char*)mess.data(), sizeof(char)*mess.size(), 0, (struct sockaddr*) &rob_si_posli, rob_slen) == -1)
        {

        }
    }
  /*  else
    {
        struct timespec t;
        RobotCommand newcommand;
        newcommand.command=command;
        newcommand.radius=radius;
        newcommand.speed=speed;
        //clock_gettime(CLOCK_REALTIME,&t);
        newcommand.timestamp=std::chrono::steady_clock::now();//(int64_t)(t.tv_sec) * (int64_t)1000000000 + (int64_t)(t.tv_nsec);
        commandQuery.push_back(newcommand);
    }*/
}
/*void MainWindow::autonomousRobotCommand(char command,double speed,int radius)
{
    return;
    std::vector<unsigned char> mess;
    switch(command)
    {
    case  ROBOT_VPRED:
        mess=robot.setTranslationSpeed(speed);
        break;
    case ROBOT_VZAD:
        mess=robot.setTranslationSpeed(speed);
        break;
    case ROBOT_VLAVO:
        mess=robot.setRotationSpeed(speed);
        break;
    case ROBOT_VPRAVO:
        mess=robot.setRotationSpeed(speed);
        break;
    case ROBOT_STOP:
        mess=robot.setTranslationSpeed(0);
        break;
    case ROBOT_ARC:
        mess=robot.setArcSpeed(speed,radius);
        break;

    }
    if (sendto(rob_s, (char*)mess.data(), sizeof(char)*mess.size(), 0, (struct sockaddr*) &rob_si_posli, rob_slen) == -1)
    {

    }
}
void MainWindow::robotexec()
{


    if(applyDelay==true)
    {
        struct timespec t;

        // clock_gettime(CLOCK_REALTIME,&t);
        auto timestamp=std::chrono::steady_clock::now();;//(int64_t)(t.tv_sec) * (int64_t)1000000000 + (int64_t)(t.tv_nsec);
        for(int i=0;i<commandQuery.size();i++)
        {
       //     std::cout<<(std::chrono::duration_cast<std::chrono::nanoseconds>(timestamp-commandQuery[i].timestamp)).count()<<std::endl;
            if((std::chrono::duration_cast<std::chrono::nanoseconds>(timestamp-commandQuery[i].timestamp)).count()>(2.5*1000000000))
            {
                char cmd=commandQuery[i].command;
                std::vector<unsigned char> mess;
                switch(cmd)
                {
                case  ROBOT_VPRED:
                    mess=robot.setTranslationSpeed(commandQuery[i].speed);
                    break;
                case ROBOT_VZAD:
                    mess=robot.setTranslationSpeed(commandQuery[i].speed);
                    break;
                case ROBOT_VLAVO:
                    mess=robot.setRotationSpeed(commandQuery[i].speed);
                    break;
                case ROBOT_VPRAVO:
                    mess=robot.setRotationSpeed(commandQuery[i].speed);
                    break;
                case ROBOT_STOP:
                    mess=robot.setTranslationSpeed(0);
                    break;
                case ROBOT_ARC:
                    mess=robot.setArcSpeed(commandQuery[i].speed,commandQuery[i].radius);
                    break;

                }
                if (sendto(rob_s, (char*)mess.data(), sizeof(char)*mess.size(), 0, (struct sockaddr*) &rob_si_posli, rob_slen) == -1)
                {

                }
                commandQuery.erase(commandQuery.begin()+i);
                i--;

            }
        }
    }
}
*/


void MainWindow::paintThisLidar(LaserMeasurement &laserData)
{
    memcpy( &paintLaserData,&laserData,sizeof(LaserMeasurement));
    updateLaserPicture=1;
    //update();
}


void MainWindow::skeletonprocess()
{

    std::cout<<"init skeleton"<<std::endl;
#ifdef _WIN32
    WSADATA wsaData = {0};
    int iResult = 0;
    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
#else
#endif
    ske_slen = sizeof(ske_si_other);
    if ((ske_s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {

    }

    char ske_broadcastene=1;
#ifdef _WIN32
    DWORD timeout=100;

    std::cout<<setsockopt(ske_s, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof timeout)<<std::endl;
    std::cout<<setsockopt(ske_s,SOL_SOCKET,SO_BROADCAST,&ske_broadcastene,sizeof(ske_broadcastene))<<std::endl;
#else
    setsockopt(ske_s,SOL_SOCKET,SO_BROADCAST,&ske_broadcastene,sizeof(ske_broadcastene));
#endif
    // zero out the structure
    memset((char *) &ske_si_me, 0, sizeof(ske_si_me));

    ske_si_me.sin_family = AF_INET;
    ske_si_me.sin_port = htons(23432);
    ske_si_me.sin_addr.s_addr = htonl(INADDR_ANY);

    ske_si_posli.sin_family = AF_INET;
    ske_si_posli.sin_port = htons(23432);
    ske_si_posli.sin_addr.s_addr = inet_addr(ipaddress.data());;//htonl(INADDR_BROADCAST);
    std::cout<<::bind(ske_s , (struct sockaddr*)&ske_si_me, sizeof(ske_si_me) )<<std::endl;;
    char command=0x00;

    skeleton bbbk;
    double measure[225];
    while(stopall==1)
    {

        if ((ske_recv_len = ::recvfrom(ske_s, (char *)&bbbk.joints, sizeof(char)*1800, 0, (struct sockaddr *) &ske_si_other, &ske_slen)) == -1)
        {

        //    std::cout<<"problem s prijatim"<<std::endl;
            continue;
        }


        memcpy(kostricka.joints,bbbk.joints,1800);
     updateSkeletonPicture=1;
  //      std::cout<<"doslo "<<ske_recv_len<<std::endl;
      //  continue;
        for(int i=0;i<75;i+=3)
        {
        //    std::cout<<klby[i]<<" "<<bbbk.joints[i].x<<" "<<bbbk.joints[i].y<<" "<<bbbk.joints[i].z<<std::endl;
        }
    }
    std::cout<<"koniec thread"<<std::endl;
}

void MainWindow::imageViewer()
{
    cv::VideoCapture cap;
    cap.open("http://127.0.0.1:8889/stream.mjpg");
    cv::Mat frameBuf;
    while(1)
    {
        cap >> frameBuf;


        if(frameBuf.rows<=0)
        {
            std::cout<<"nefunguje"<<std::endl;
            continue;
        }

        if(applyDelay==true)
        {
            struct timespec t;
            CameraVector newcommand;
            frameBuf.copyTo(newcommand.data);
            //    clock_gettime(CLOCK_REALTIME,&t);
            newcommand.timestamp=std::chrono::steady_clock::now();//(int64_t)(t.tv_sec) * (int64_t)1000000000 + (int64_t)(t.tv_nsec);
            auto timestamp=std::chrono::steady_clock::now();//(int64_t)(t.tv_sec) * (int64_t)1000000000 + (int64_t)(t.tv_nsec);
            cameraQuerry.push_back(newcommand);
            for(int i=0;i<cameraQuerry.size();i++)
            {
                if((std::chrono::duration_cast<std::chrono::nanoseconds>(timestamp-cameraQuerry[i].timestamp)).count()>(2.5*1000000000))
                {

                    cameraQuerry[i].data.copyTo(robotPicture);
                    cameraQuerry.erase(cameraQuerry.begin()+i);
                    i--;
                    break;

                }
            }

        }
        else
        {


           frameBuf.copyTo(robotPicture);
        }
        frameBuf.copyTo(AutonomousrobotPicture);
        updateCameraPicture=1;

        update();
        //std::cout<<"vycital som"<<std::endl;
       // cv::imshow("client",frameBuf);
        cv::waitKey(1);
        QCoreApplication::processEvents();
    }
}

void MainWindow::on_pushButton_7_toggled(bool checked)
{
    if(checked){
        char tt=0x00;
        pohyb = 0;
        keyboard_control = 0;
        sendRobotCommand(tt);
        safety_stop = 1;
    }else{
        safety_stop = 0;
    }
}

